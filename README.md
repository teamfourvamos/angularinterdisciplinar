Para ejecutar la aplicacion necesitara seguir los siguientes pasos:
    1-Vayase a la terminal en el software Visual Studio Code
    2- Escriba "cd proyectointerdisciplinar"
    3- Escriba "npm install"
    4- Escriba "ng serve"
    5- Abra su navegador(preferiblemente google chrome)
    4- Escriba en la url "localhost:4200/"
    5-Haga el formulario de login con usuario: "prueba@prueba" y password"123"
    6-Haga click en un enlace y recargue su pagina

Este proyecto consume los recursos de su api correspondiente, para hecho, descargue la api llamada proyectointerdisciplinar
que tiene a su disposicion, y siga los pasos contenidos en el fiche Readme propio de la api.

Sofware necesario:
    -Visual Studio Code
    -Framework Spring
    -Postman(recomendado)
Esta aplicacion esta compuesta del API, programada en Spring y en lenguaje JAVA. la consumicion de recursos
corre por parte de esta aplicacion Angular

Materiales externos utilizados para el desarrollo de la parte visual de la aplicacion:
    -https://material.io/
    -https://material.angular.io/

Especiales agradecimientos a:
    -Luis Miguel Lopez Magaña(Nuestra guia para con la Api)
    -Miguel Campos(Nuestra guia en Angular)


import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {MatTableModule} from '@angular/material/table';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule } from '@angular/material/card';
import { Routes, RouterModule } from '@angular/router';
import { PanelControlSuperAdminComponent } from './panel-control-super-admin/panel-control-super-admin.component';
import { ListaAnioComponent } from './lista-anio/lista-anio.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';


import { FormAnioComponent } from './form-anio/form-anio.component';
import { LoginComponent } from './login/login.component';

import { CrearProcesoComponent } from './crear-proceso/crear-proceso.component';
import { ListaProcesoComponent } from './lista-proceso/lista-proceso.component';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import {MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS} from '@angular/material/snack-bar';

import { CreaColegioComponent } from './crea-colegio/crea-colegio.component';
import { SelectAnioComponent } from './select-anio/select-anio.component';
import { ListEtapasComponent } from './list-etapas/list-etapas.component';

import { ListadoColegioComponent } from './listado-colegio/listado-colegio.component';
import { ListadoUnidadComponent } from './listado-unidad/listado-unidad.component';
import { FormUnidadComponent } from './form-unidad/form-unidad.component';

import { CrearAdminComponent } from './crear-admin/crear-admin.component';


export function tokenGetter() {
  return localStorage.getItem("token");
}

const routes: Routes = [ 
  { path: 'anios', component:ListaAnioComponent  },
  { path: 'procesos', component:ListaProcesoComponent},
  { path: 'crearproceso', component:CrearProcesoComponent},
  {path: 'nuevoAnio', component: FormAnioComponent},
  {path: 'login',component:LoginComponent},
  {path: 'usuario/listadoUsuarios',component:PanelControlSuperAdminComponent},

  {path: 'crearcolegio',component:CreaColegioComponent},
  {path: 'etapas',component:SelectAnioComponent},
  {path: 'etapas/listar',component:ListEtapasComponent},

  {path: 'colegios',component:ListadoColegioComponent},
  {path: 'unidades',component:ListadoUnidadComponent},
  {path: 'nuevaUnidad',component:FormUnidadComponent},
  {path: 'crear-admin', component:CrearAdminComponent}
  

];
  

@NgModule({
  declarations: [
    AppComponent,
    PanelControlSuperAdminComponent,
    ListaAnioComponent, 
    FormAnioComponent, 
    LoginComponent,  CrearProcesoComponent, ListaProcesoComponent, CreaColegioComponent, SelectAnioComponent, ListEtapasComponent,
     ListadoColegioComponent, ListadoUnidadComponent, FormUnidadComponent,CrearAdminComponent

  ],
  imports: [
    BrowserModule,
    MatSnackBarModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatTableModule,
    MatCardModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ["example.com"],
        blacklistedRoutes: ["example.com/examplebadroute/"]
      }
    }),
    MatToolbarModule,
    MatIconModule,
    RouterModule.forRoot(
      routes
    )
    
  ],
  providers: [

    JwtHelperService,
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 5000 } },


  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

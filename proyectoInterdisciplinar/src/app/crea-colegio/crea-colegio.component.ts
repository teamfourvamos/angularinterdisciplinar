import { Component, OnInit } from '@angular/core';
import { NuevoColegioDto } from '../models/nuevoColegio.dto';
import { ColegioService } from '../services/colegio.service';

@Component({
  selector: 'app-crea-colegio',
  templateUrl: './crea-colegio.component.html',
  styleUrls: ['./crea-colegio.component.css']
})
export class CreaColegioComponent implements OnInit {
  colegio:NuevoColegioDto;
  
 
  constructor(private colegioServicio: ColegioService) { 
    this.colegio = new NuevoColegioDto('');
  }


  ngOnInit() {
  }

  doColegio(){
     this.colegioServicio.nuevoColegio(this.colegio).subscribe(resp =>{
      alert('Has creado el colegio '+resp.nombre);
      window.location.href = 'http://localhost:4200'
     });
  }

}

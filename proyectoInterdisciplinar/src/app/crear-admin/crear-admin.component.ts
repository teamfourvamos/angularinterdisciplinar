import { Component, OnInit } from '@angular/core';
import { NuevoAdminDto } from '../dto/Crear-admin-dto';
import { ColegioResponse } from '../models/Colegio-response-interface';
import { UsuarioService } from '../services/usuario.service';
import { ColegioService } from '../services/colegio.service';
import { AdminDtoResponse } from '../dto/Crear-admin-dto.response';

@Component({
  selector: 'app-crear-admin',
  templateUrl: './crear-admin.component.html',
  styleUrls: ['./crear-admin.component.css']
})
export class CrearAdminComponent implements OnInit {
  admin:NuevoAdminDto;
  colegios: ColegioResponse[];
  constructor(private usuarioService: UsuarioService, private colegioService:ColegioService) { 
    this.admin = new NuevoAdminDto('','','',null,'','',null);
  }

  ngOnInit() {
    this.cargarListadoColegios();
  }

  doAdmin(){

    this.usuarioService.nuevoAdmin(this.admin).subscribe(resp =>{
      alert('Has creado un nuevo admin');
      window.location.href = 'http://localhost:4200/usuario/'
    });
  }

  cargarListadoColegios(){
    this.colegioService.getColegios().subscribe(resp => {
      this.colegios = resp;
    });

  }

}

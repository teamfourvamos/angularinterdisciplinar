import { Component, OnInit } from '@angular/core';
import { ProcesoService } from '../services/proceso.service';
import { NuevoProcesoDto } from '../models/nuevoProceso-dto.interface';
import { AnioAcademico } from '../models/AnioAcademico-interface';
import { AnioserviceService } from '../services/anioservice.service';

@Component({
  selector: 'app-crear-proceso',
  templateUrl: './crear-proceso.component.html',
  styleUrls: ['./crear-proceso.component.css']
})
export class CrearProcesoComponent implements OnInit {

    proceso:NuevoProcesoDto;
    anios:AnioAcademico[];
  constructor(private procesoService: ProcesoService,
    private anioService: AnioserviceService) { 
    this.proceso = new NuevoProcesoDto('','', null,null);
  }

  ngOnInit() {
    this.loadAnios();
  }

  doProceso(){

    this.procesoService.nuevoProceso(this.proceso).subscribe(resp =>{
      alert('Has creado el proceso '+resp.nombre);
      window.location.href = 'http://localhost:4200/procesos/'
    });
  }
  loadAnios(){
    this.anioService.getAnios().subscribe(resp =>{
      this.anios=resp
    });
  }

  
  

  


}

export class AdminDtoResponse{
    id:number;
    password:string;
    password2:string;
    nombre:string;
    apellidos:string;
    idcolegio:number;
    roles:Set<String>;
}
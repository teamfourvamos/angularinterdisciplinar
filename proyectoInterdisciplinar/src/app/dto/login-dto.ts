export class LoginDto {

    constructor(public email: string, public password: string, public grant_type:string) {
    }
}
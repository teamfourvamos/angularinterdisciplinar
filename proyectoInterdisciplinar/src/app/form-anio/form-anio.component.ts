import { Component, OnInit } from '@angular/core';
import { AnioserviceService } from '../services/anioservice.service';
import { NuevoAnioDto } from '../models/nuevoAnio-dto';

@Component({
  selector: 'app-form-anio',
  templateUrl: './form-anio.component.html',
  styleUrls: ['./form-anio.component.css']
})
export class FormAnioComponent implements OnInit {

  anio:NuevoAnioDto;

constructor(private anioService: AnioserviceService) { 
  this.anio = new NuevoAnioDto('', '');
}

  ngOnInit() {
  }

  doAnio(){

    this.anioService.nuevoAnio(this.anio).subscribe(resp =>{
      alert('Has creado el anio'+resp.yearI+resp.yearF);
      window.location.href = 'http://localhost:4200/anios/'
    });
  }

  

  
}

import { Component, OnInit } from '@angular/core';
import { NuevaUnidadDto } from '../models/nuevaUnidad-dto';
import { UnidadService } from '../services/unidad.service';

@Component({
  selector: 'app-form-unidad',
  templateUrl: './form-unidad.component.html',
  styleUrls: ['./form-unidad.component.css']
})
export class FormUnidadComponent implements OnInit {

  unidad:NuevaUnidadDto;

  constructor(private unidadService: UnidadService) { 
    this.unidad = new NuevaUnidadDto('','',null);
  }

  ngOnInit() {
  }

  doUnidad(){

    this.unidadService.nuevaUnidad(this.unidad).subscribe(resp =>{
      alert('Has creado la unidad'+resp.nombre);
      window.location.href = 'http://localhost:4200/nuevaUnidad/'
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { EtapaService } from '../services/etapa.service';
import { Etapa } from '../models/Etapa-interface';

@Component({
  selector: 'app-list-etapas',
  templateUrl: './list-etapas.component.html',
  styleUrls: ['./list-etapas.component.css']
})
export class ListEtapasComponent implements OnInit {

  etapas: Etapa[];
  columnsToDisplay = ['nombre',"delete"];

  constructor( private etapaService:EtapaService) { }

  ngOnInit() {
    this.loadEtapas();
  }
  loadEtapas(){
    this.etapaService.getEtapas().subscribe(resp =>{
      this.etapas=resp
    });
  }
  public deleteEtapa(etapaId:number){
    this.etapaService.deleteDeleteEtapa(etapaId).subscribe(resp =>{
      window.location.href = 'http://localhost:4200/etapas/listar'
  
    });
  }


}

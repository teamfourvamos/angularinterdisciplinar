import { Component, OnInit } from '@angular/core';
import { AnioserviceService } from '../services/anioservice.service';
import { AnioAcademico } from '../models/AnioAcademico-interface';
import { ANIOS } from '../models/mock-anio';

@Component({
  selector: 'app-lista-anio',
  templateUrl: './lista-anio.component.html',
  styleUrls: ['./lista-anio.component.css']
})
export class ListaAnioComponent implements OnInit {

  anios:AnioAcademico[];
  columnsToDisplay = ['id','yearI',"yearF","delete"];

  
  constructor(
    private anioService: AnioserviceService

  ) { }

  ngOnInit() {
    this.loadAnios();
  }

  loadAnios(){
    this.anioService.getAnios().subscribe(resp =>{
      this.anios=resp
    });
  }


public deleteAnio(anioId:number){
  this.anioService.deleteDeleteAnio(anioId).subscribe(resp =>{
    window.location.href = 'http://localhost:4200/anios/'

  });
}

doAnadir(){

   
  window.location.href = 'http://localhost:4200/nuevoAnio'

}


}

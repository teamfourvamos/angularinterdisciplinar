import { Component, OnInit } from '@angular/core';
import { ProcesoService } from '../services/proceso.service';
import { Proceso } from '../models/Proceso-interface';
import { AnioAcademico } from '../models/AnioAcademico-interface';

@Component({
  selector: 'app-lista-proceso',
  templateUrl: './lista-proceso.component.html',
  styleUrls: ['./lista-proceso.component.css']
})
export class ListaProcesoComponent implements OnInit {
  procesos:Proceso[] ;

  columnsToDisplay = ['nombre',"tipo","delete"];
  
  constructor(
    private procesoService: ProcesoService
  ) { }

  ngOnInit() {
    this.loadProcesos();
  }

  loadProcesos(){
    this.procesoService.getProcesos().subscribe(resp =>{
      this.procesos=resp
    });
  }
  public deleteProceso(procesoId:number){
    this.procesoService.deleteDeleteProceso(procesoId).subscribe(resp =>{
      window.location.href = 'http://localhost:4200/procesos/'
  
    });
  }

  doAnadir(){

   
    window.location.href = 'http://localhost:4200/crearproceso'
  
}

}

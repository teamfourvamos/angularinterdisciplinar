import { Component, OnInit } from '@angular/core';
import { ColegioService } from '../services/colegio.service';
import { ColegioResponse } from '../models/Colegio-response-interface';

@Component({
  selector: 'app-listado-colegio',
  templateUrl: './listado-colegio.component.html',
  styleUrls: ['./listado-colegio.component.css']
})
export class ListadoColegioComponent implements OnInit {
  colegios:ColegioResponse[];
  columnsToDisplay = ['id','nombre'];
  constructor(private colegioServicio : ColegioService) { 

  }

  ngOnInit() {
    this.cargarListadoColegios();
  }

  cargarListadoColegios(){
    this.colegioServicio.getColegios().subscribe(resp => {
      this.colegios = resp;
    });

  }
}
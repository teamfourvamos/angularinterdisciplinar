import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoUnidadComponent } from './listado-unidad.component';

describe('ListadoUnidadComponent', () => {
  let component: ListadoUnidadComponent;
  let fixture: ComponentFixture<ListadoUnidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadoUnidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoUnidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

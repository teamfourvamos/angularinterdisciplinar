import { Component, OnInit } from '@angular/core';
import { Unidad } from '../models/Unidad-interface';
import { UnidadService } from '../services/unidad.service';

@Component({
  selector: 'app-listado-unidad',
  templateUrl: './listado-unidad.component.html',
  styleUrls: ['./listado-unidad.component.css']
})
export class ListadoUnidadComponent implements OnInit {
  unidades:Unidad[];

  columnsToDisplay = ['nombre',"tipo","delete"];

  constructor(private unidadService: UnidadService) { 
    
  }

  ngOnInit() {
    this.loadUnidades()
  }

  loadUnidades(){
    this.unidadService.getUnidades().subscribe(resp =>{
      this.unidades=resp
    });
  }

  public deleteUnidad(procesoId:number){
    this.unidadService.deleteDeleteUnidad(procesoId).subscribe(resp =>{
      window.location.href = 'http://localhost:4200/unidades/'
  
    });
  }

  doAnadir(){

   
    window.location.href = 'http://localhost:4200/nuevaUnidad'
  
  }

}

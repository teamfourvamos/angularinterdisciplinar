import { Component, OnInit } from '@angular/core';
import { LoginDto } from '../dto/login-dto';
import { AuthService } from '../services/auth.service';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  usuario: LoginDto;

  constructor(private authService: AuthService, private router:Router, private snackBar: MatSnackBar, private jwtHelper: JwtHelperService) { 
    this.usuario = new LoginDto('', '','password');
  }

  ngOnInit() {
  }

  doLogin() {
      this.authService.login(this.usuario).subscribe(resul => {
        this.authService.setToken(resul.access_token);
        this.authService.setTokenRefres(resul.refresh_token);
        console.log(this.jwtHelper.decodeToken(this.authService.getToken()));
        this.router.navigate(['/anios']);
        
      },
      error => {
          this.snackBar.open('Authentication failed.');
      });
    } 
}

import { Etapa } from './Etapa-interface';
import { Psm } from './Psm-interface';
import { Usuario } from './Usuario-interface';
import { IndicadorCentro } from './IndicadorCentro-interface';
import { ValorIndicadorCentro } from './ValorIndicadorCentro-interface';

export interface Colegio {
    id: number;
    nombre: string;
    etapa: any[];
    valorIndicadoCentro: any[];
    indicadorCentroColegio: any[];
    psm: any[];
}
import { Etapa } from './Etapa-interface';
import { Unidad } from './Unidad-interface';

export class Curso{

    id:number;
    unidad:Unidad[];
    etapa:Etapa[];
    nombre:String;
    peso:number;


}
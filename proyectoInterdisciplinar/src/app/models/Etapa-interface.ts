import { Colegio } from './Colegio-interface';
import { AnioAcademico } from './AnioAcademico-interface';
import { Curso } from './Curso-interface';

export interface Etapa{

    id: number;
    nombre: string;
    
    peso: number;
 
    
    
}
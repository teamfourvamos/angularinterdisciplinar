import { Proceso } from './Proceso-interface';

export class Indicador{

    id:number;
    nombre:String;
    exPorcentaje:boolean;
    proceso:Proceso;

}
import { Colegio } from './Colegio-interface';
import { Indicador } from './Indicador-interface';
import { ValorIndicadorCentro } from './ValorIndicadorCentro-interface';

export class IndicadorCentro extends Indicador{

    valorIndicadorCentro:ValorIndicadorCentro[];
    colegio:Colegio[];
}
import { Indicador } from './Indicador-interface';
import { Unidad } from './Unidad-interface';
import { ValorIndicadorUnidad } from './ValorIndicadorUnidad-interface';

export class IndicadorUnidad extends Indicador{

    unidad:Unidad[];
    valorIndicadorUnidad: ValorIndicadorUnidad[];

}
import { Colegio } from './Colegio-interface';
import { ValorIndicador } from './ValorIndicador-interface';
import { PuntoControl } from './PuntoControl-interface';

export class Psm{

    id:number;
    nombre:String;
    evaluacion:Evaluacion;
    peso:number;
    colegio:Colegio;
    puntoControl:PuntoControl;
    valorIndicador:ValorIndicador[];
}
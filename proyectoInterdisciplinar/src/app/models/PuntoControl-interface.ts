import { Colegio } from './Colegio-interface';
import { Psm } from './Psm-interface';

export class PuntoControl{

    id:number;
    fecha:String;
    colegio:Colegio;
    psm:Psm[];
}
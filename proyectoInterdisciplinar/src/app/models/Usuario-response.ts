import { Usuario } from './Usuario-interface';
import { Colegio } from './Colegio-interface';

export interface UsuarioResponse {
    username: string;
    id: number;
    roles: any[];
    nombre: string;
    apellidos: string;
    colegio: any;
    email:string;
    password:string;
}

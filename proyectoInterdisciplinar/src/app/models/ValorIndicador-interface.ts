import { Psm } from './Psm-interface';

export class ValorIndicador{
    id:number;
    valorReal:number;
    valorConforme:number;
    esNoAplica:boolean;
    psm:Psm;
}
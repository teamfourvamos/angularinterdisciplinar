import { ValorIndicador } from './ValorIndicador-interface';
import { Colegio } from './Colegio-interface';
import { IndicadorCentro } from './IndicadorCentro-interface';

export class ValorIndicadorCentro extends ValorIndicador{
    
    colegio:Colegio;
    indicadorCentro:IndicadorCentro;


}
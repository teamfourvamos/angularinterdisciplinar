import { ValorIndicador } from './ValorIndicador-interface';
import { Unidad } from './Unidad-interface';
import { IndicadorUnidad } from './IndicadorUnidad-interface';

export class ValorIndicadorUnidad extends ValorIndicador{

    unidad:Unidad;
    indicadorUnidad:IndicadorUnidad;
}
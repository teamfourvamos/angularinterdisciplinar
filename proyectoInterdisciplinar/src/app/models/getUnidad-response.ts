import { Curso } from './Curso-interface';
import { ValorIndicadorUnidad } from './ValorIndicadorUnidad-interface';
import { IndicadorUnidad } from './IndicadorUnidad-interface';

export interface GetUnidadResponse {
    id:number;
    curso:Curso;
    nombre:String;
    peso:number;
    valorIndicadorUnidad:ValorIndicadorUnidad[];
    indicadorUnidad:IndicadorUnidad[];
 
}
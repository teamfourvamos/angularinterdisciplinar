import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { Usuario } from '../models/Usuario-interface';
import { UsuarioResponse } from '../models/Usuario-response';

@Component({
  selector: 'app-panel-control-super-admin',
  templateUrl: './panel-control-super-admin.component.html',
  styleUrls: ['./panel-control-super-admin.component.css']
})
export class PanelControlSuperAdminComponent implements OnInit {
  usuarios:Usuario[];
  columnsToDisplay = ['id','nombre','apellidos','email','colegio','delete'];
  constructor(private usuarioServicio:UsuarioService) { 

  }

  ngOnInit() {
    this.cargarListadoUsuarios();
  }

  cargarListadoUsuarios(){
    this.usuarioServicio.getUsuarios().subscribe(resp => {
      this.usuarios = resp;
    });
  }

  public deleteUser(usuarioId:number){
    this.usuarioServicio.deleteDeleteUsuario(usuarioId).subscribe(resp =>{
      window.location.href = 'http://localhost:4200/usuario/listadoUsuarios'
  
    });
  }

}

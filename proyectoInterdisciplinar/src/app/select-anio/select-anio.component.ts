import { Component, OnInit } from '@angular/core';
import { AnioserviceService } from '../services/anioservice.service';
import { AnioAcademico } from '../models/AnioAcademico-interface';
import { AnioSelectDto } from '../models/anioselect.dto';

@Component({
  selector: 'app-select-anio',
  templateUrl: './select-anio.component.html',
  styleUrls: ['./select-anio.component.css']
})
export class SelectAnioComponent implements OnInit {
  anyo:AnioSelectDto;
  anios:AnioAcademico[];
  constructor(private anioService: AnioserviceService) { 

    this.anyo = new AnioSelectDto(null);
  }

  ngOnInit() {
    this.loadAnios();
  }

  loadAnios(){
    this.anioService.getAnios().subscribe(resp =>{
      this.anios=resp
    });
  }
  doSelectAnio(){
    localStorage.setItem('anioId',this.anyo.id.toString());
    window.location.href = 'http://localhost:4200/etapas/listar'
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GetAnioResponse } from '../models/getAnio-response.interface';
import { Observable } from 'rxjs';
import { DeleteAnioResponse } from '../models/deleteanio-response.interface';
import { NuevoAnioDto } from '../models/nuevoAnio-dto';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'Bearer '+ localStorage.getItem('token')
  })
};
@Injectable({
  providedIn: 'root'
})
export class AnioserviceService {

  constructor(private http: HttpClient) { }




  public getAnios():Observable<GetAnioResponse[]>{
    return this.http.get<GetAnioResponse[]>(
      'http://localhost:9000/anio/',
      httpOptions
    );

  }

  deleteDeleteAnio(anioId: number):Observable<DeleteAnioResponse>{
    return this.http.delete<DeleteAnioResponse>(
      'http://localhost:9000/anio/'+anioId,
      httpOptions
    );
  }

  nuevoAnio(newAnioDto: NuevoAnioDto): Observable<GetAnioResponse> {
    return this.http.post<GetAnioResponse>(
      URL+'anio/',
      newAnioDto,
      httpOptions
    );
  }
  
 
}

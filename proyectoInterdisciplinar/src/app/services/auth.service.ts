import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { LoginDto } from '../dto/login-dto';
import { LoginResponse } from '../models/login-response';

const LOGIN_URL = 'localhost:9000/oauth/token?';

const httpOptions = {//application/x-www-form-urlencoded
  headers: new HttpHeaders().append('Authorization',
    'Basic ' + btoa(`proyectoInter:secret`)).append('Content-type','application/x-www-form-urlencoded')
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(loginDto: LoginDto): Observable<LoginResponse> {
    const params = new HttpParams()
    .set('username', loginDto.email)
    .set('password', loginDto.password)
    .set('grant_type', 'password');
    return this.http.post<LoginResponse>('http://localhost:9000/oauth/token', params,
      httpOptions
);
}

public getToken(): string {
  return localStorage.getItem("token");
}

public setToken(token: string) {
  localStorage.setItem("token", token);
}

public getTokenRefres(): string {
  return localStorage.getItem("tokenRefres");
}

public setTokenRefres(token: string) {
  localStorage.setItem("tokenRefres", token);
}

public clearToken() {
  localStorage.removeItem("tokenRefres");
  localStorage.removeItem("token");
}

}


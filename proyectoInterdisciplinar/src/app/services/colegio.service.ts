import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NuevoColegioDto } from '../models/nuevoColegio.dto';
import { NewColegioResponse } from '../models/newColegio-response.interface';

const URL='http://localhost:9000/';

import { ColegioResponse } from '../models/Colegio-response-interface';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'Bearer '+ localStorage.getItem('token')
  })
};
@Injectable({
  providedIn: 'root'
})
export class ColegioService {


  constructor(private http: HttpClient) { }

  nuevoColegio  (newColegioDto: NuevoColegioDto): Observable<NewColegioResponse> {
    return this.http.post<NewColegioResponse>(
      URL+'colegios/',
      newColegioDto,
      httpOptions
    );
  }

    getColegios(): Observable<ColegioResponse[]>{
      return this.http.get<ColegioResponse[]>(
        'http://localhost:9000/colegios/listadoColegio',
        httpOptions
      );
  
    }

 
}

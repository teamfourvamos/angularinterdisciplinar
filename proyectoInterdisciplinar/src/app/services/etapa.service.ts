import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetEtapaResponse } from '../models/getEtapas-response.interface';
import { DeleteEtapaResponse } from '../models/deleteEtapa-respone.interface';

const URL='http://localhost:9000/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'Bearer '+ localStorage.getItem('token')
  })
};
@Injectable({
  providedIn: 'root'
})
export class EtapaService {

  constructor(private http: HttpClient) { }


  public getEtapas():Observable<GetEtapaResponse[]>{
    return this.http.get<GetEtapaResponse[]>(
     URL+'etapas/'+localStorage.getItem('anioId'),
      httpOptions
    );

  }


  deleteDeleteEtapa(EtapaId: number):Observable<DeleteEtapaResponse>{
    return this.http.delete<DeleteEtapaResponse>(
      URL+'etapas/'+EtapaId,
      httpOptions
    );
  }
}

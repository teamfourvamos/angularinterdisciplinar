import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetProcesoResponse } from '../models/getProceso-response-interface';
import { DeleteProcesoResponse } from '../models/deleteProceso-response-interface';
import { NuevoProcesoDto } from '../models/nuevoProceso-dto.interface';

const URL='http://localhost:9000/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'Bearer '+ localStorage.getItem('token')
  })
};
@Injectable({
  providedIn: 'root'
})
export class ProcesoService {

  constructor(private http: HttpClient) { }



  public getProcesos():Observable<GetProcesoResponse[]>{
    return this.http.get<GetProcesoResponse[]>(
     URL+'procesos/',
      httpOptions
    );

  }

  deleteDeleteProceso(procesoId: number):Observable<DeleteProcesoResponse>{
    return this.http.delete<DeleteProcesoResponse>(
      URL+'procesos/'+procesoId,
      httpOptions
    );
  }

  nuevoProceso(newProcesoDto: NuevoProcesoDto): Observable<GetProcesoResponse> {
    return this.http.post<GetProcesoResponse>(
      URL+'procesos/',
      newProcesoDto,
      httpOptions
    );
  }
}

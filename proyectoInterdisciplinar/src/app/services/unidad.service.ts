import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetUnidadResponse } from '../models/getUnidad-response';
import { DeleteUnidadResponse } from '../models/deleteUnidad-response';
import { NuevaUnidadDto } from '../models/nuevaUnidad-dto';

const URL='http://localhost:9000/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'Bearer '+ localStorage.getItem('token')
  })
};
@Injectable({
  providedIn: 'root'
})
export class UnidadService {

  constructor(private http: HttpClient) { }

  public getUnidades():Observable<GetUnidadResponse[]>{
    return this.http.get<GetUnidadResponse[]>(
     URL+'procesos/',
      httpOptions
    );

  }

  deleteDeleteUnidad(procesoId: number):Observable<DeleteUnidadResponse>{
    return this.http.delete<DeleteUnidadResponse>(
      URL+'procesos/'+procesoId,
      httpOptions
    );
  }

  nuevaUnidad(newUnidadDto: NuevaUnidadDto): Observable<GetUnidadResponse> {
    return this.http.post<GetUnidadResponse>(
      URL+'procesos/',
      newUnidadDto,
      httpOptions
    );
  }






}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NuevoAdminDto } from '../dto/Crear-admin-dto';
import { UsuarioResponse } from '../models/Usuario-response';
import { DeleteUsuarioResponse } from '../models/Deleteusuario-response-interface';
import { AdminDtoResponse } from '../dto/Crear-admin-dto.response';
 const URL = 'http://localhost:9000/usuario/listadoUsuarios';
 const apiKey = "";
 const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'Bearer '+ localStorage.getItem('token')
  })
};

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) {

   }

  public getUsuarios(): Observable<UsuarioResponse[]>{
    return this.http.get<UsuarioResponse[]>(
      URL,
      httpOptions
    );


  } 
  deleteDeleteUsuario(usuarioId: number):Observable<DeleteUsuarioResponse>{
    return this.http.delete<DeleteUsuarioResponse>(
      'http://localhost:9000/usuario/'+usuarioId,
      httpOptions
    );
  }

  nuevoAdmin(nuevoAdminDto: NuevoAdminDto): Observable<AdminDtoResponse> {
    return this.http.post<AdminDtoResponse>(
      'http://localhost:9000/usuario/registroAdmin',
      nuevoAdminDto,
      httpOptions
    );
  }

}
